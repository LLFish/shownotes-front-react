import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import showdown from 'showdown';


function markdown_init(){
  showdown.setFlavor('github');
  showdown.setOption('tables', true);
  showdown.setOption('tasklists', true);
}

try {
  markdown_init();
} catch {
  console.error("markdown 初始化失败");
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
