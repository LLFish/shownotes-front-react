import React from "react";
import {
  HashRouter as Router,
  Route,
} from "react-router-dom";
import { Footer } from "./pages/Footer";
import { Header } from "./pages/Header";
import { NavSecond } from "./pages/NavSecond";
import { Main } from "./pages/Main";
import { DefaultMain } from "./pages/DefaultMain";
import { Search } from "./pages/Search";

export default class App extends React.Component{
  constructor(props){
    super(props);
    this.state = {fileName:''};
  }

  handleNameChange = (fileName) => {
    if(!fileName || typeof fileName != 'string'){
      console.warn("useless filename: ", fileName);
      return;
    }
    console.log(fileName);
    this.setState({fileName});
  }

  render(){
      return (
        <Router>
          <div id='wrapper'>
            <Header/>
            <NavSecond handleNameChange={this.handleNameChange}/>
            <Route path="/file/:fileName" component={Main}/>
            <Route path="/search/:content" component={Search}/>
            <Route exact path="/" component={DefaultMain} />
            <Footer/>
          </div>
        </Router>
      );
    }    
  }

