import axios from "axios";
import React from "react";
import showdown from "showdown";
import { Comment } from "./Comment";

export class Main extends React.Component {

    componentDidUpdate(prevProps){
        if(!this.props|| !this.props.match || !this.props.match.params) {
            return ;
        }
        let preFileName = prevProps.match?.params?.fileName;
        const fileName = this.props.match.params.fileName || null;
        if(fileName === preFileName || !fileName){
            return ;
        }
        
        axios.get(`/files/file?name=${fileName}`).then(data =>{
            let {name, content, success} = {...data.data};
            this.setState({name, content, success});
        });   
    }

    componentDidMount(){
        if(!this.props|| !this.props.match || !this.props.match.params) {
            return ;
        }
        const fileName = this.props.match.params.fileName || null;
        axios.get(`/files/file?name=${fileName}`).then(data =>{
            let {name, content, success} = {...data.data};
            this.setState({name, content, success});
        });
    }

    render(){
        if(!this.state) {
            return null;
        }

        if(!this.state.success) {
            return <h1 id='main' className='right'>骚瑞，没找到这个文档</h1>;
        }

        const {name, content} = {...this.state};

        const converter = new showdown.Converter();
        let mdNodeStr = converter.makeHtml(content);
        mdNodeStr = `<h1 style="text-align:center">${name}</h1> ${mdNodeStr}`;
        return (
                <React.Fragment>
                    <div id="main" className="right">
                        <div className='main-content'
                            dangerouslySetInnerHTML={{__html:mdNodeStr}}>
                        </div>
                        <Comment fileName={name}></Comment>
                    </div>
                </React.Fragment>
        );
    }
}