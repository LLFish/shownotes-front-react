import React from "react";
import axios from "axios"
import { Link } from "react-router-dom";

export class NavSecond extends React.Component {
    constructor(props) {
        super(props);

        this.state = {names:[]};
    }

    componentDidMount(){
        this.init();
    }

    init(){
        axios.get("/files/overview").then(data =>{
            const names =  data.data;
            this.setState({names});
        })
    }

    handleNameChange = (e) =>{
        const name = e.target.innerHTML;
        this.props.handleNameChange(name);
    }

    render(){
        if(!this.state || !this.state.names ){
            return null;
        }

        const lis = this.state.names.map(name => 
            <li key={name} >
                <Link to={`/file/${name}`}>
                    <div>{name}</div>
                </Link>
            </li>);
        

        return (
            <div id="nav-second" className="left">
                <ul> {lis} </ul>
            </div>
        );
    }

}