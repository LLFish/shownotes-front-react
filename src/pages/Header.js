import React from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";

class HeaderNative extends React.Component {
    constructor(props) {
        super(props);
        this.state = {content:''};
    }

    handleChange = (e) => {
        const content = e.target.value;
        this.setState({content});
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if(!this.state.content) {
            return ;
        }
        this.props.history.push(`/search/${this.state.content}`);
    }

    render() {
        return (
            <header>
                <Link to='' className='left'>
                    <img src="/assets/llfish.png" alt="LLFish" />
                </Link>
    
                <div id="search-ui" className="right">
                    <form id="search-ui" action="search">
                        <input 
                            // type="text" 
                            value={this.state.content}
                            onChange={this.handleChange}
                            name="content" 
                            className="search-box" 
                        />
                        <input 
                            type="submit" 
                            value="Go"
                            onClick={this.handleSubmit} 
                            className="search-button" 
                        />
                    </form>
                </div>
            </header>
        );
    }
}

export const Header = withRouter(HeaderNative);