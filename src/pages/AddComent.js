import axios from "axios";
import react from "react";

export class AddComment extends react.Component {
    constructor(props) {
        super(props);
        this.state={
            fileName:this.props.fileName,
            content:'',
            author:'',
        };
    }

    handleVarChange = (varName) => {
        return ((e) => {
            const value = e.target.value;
            let obj ={};
            obj[varName] = value;
            this.setState(obj);
        });
    }

    afterSubmit = () =>{
        this.setState({content:'', author:''});
        this.props.afterSubmit();
    }

    handlSubmit= () => {
        let {content=undefined, fileName=undefined, author=undefined}= {...this.state};
        axios.post('/comments/comment', 
            {content, fileName, author}, 
            {headers:{"x-http-method-override":"POST"}}
        ).then(r => {
            if(!r.data.success) {
                alert("提交失败");
            } else {
                alert("提交成功");
                this.afterSubmit();
            }
        });
    }

    render() {
        return (
            <div id='comment-add'>
                <p id='comment-add-title'>我要发表看法</p>
                <div id = 'comment-add-content'>
                    <p className='comment-input-title'>您的留言：</p>
                    <textarea 
                        onChange={this.handleVarChange('content')}
                        spellCheck='false'
                        wrap='hard'
                        value={this.state.content}
                    ></textarea>
                </div>

                <div id='comment-add-author'>
                    <p className='comment-input-title'>您的大名：</p>
                    <input 
                        onChange={this.handleVarChange('author')} 
                        value={this.state.author}
                    ></input>
                    <span className='comment-input-tips'> --- 必填</span>
                </div>

                <div id='comment-add-submit'>
                    <input
                        type="button" 
                        value="发表"
                        onClick={this.handlSubmit}
                    ></input>
                    <span className='comment-input-tips'> --- 点击按钮</span>
                </div>
            </div>
        );
    }
}