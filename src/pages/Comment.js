import axios from "axios";
import react from "react";
import { AddComment } from "./AddComent";
import ('../assets/iconfont.css')

export class Comment extends react.Component {
    constructor(props) {
        super(props);
        this.state = {success:false};
    }

    getAndSetComments = () => {
        if(!this.props.fileName){
            return;
        }
        axios.get(`comments/comment?fileName=${this.props.fileName}`)
            .then(r => this.setState(r.data));        
    }

    componentDidMount() {
        this.getAndSetComments();
    }

    componentDidUpdate(prevProps) {
        if(this.props.fileName === prevProps.fileName){
            return ;
        }

        this.getAndSetComments();
    }

    doAppraise = (e, like) => {
        let content = like ? '1' : '0';
        return (e) => {
            
            e.cancelBubble = false;
            let id = e.target.parentNode.id || e.target.id;

            console.log(content, id);

            axios.get(`/comments/appraise?like=${content}&id=${id}`)
                .then(data => {
                    this.getAndSetComments();
                });
        }
    }

    render(){
        if(!this.state.success) {
            return null;
        }

        const comments = [];
        comments.push(...this.state.data);

        const commentNodes = comments.map((v, index) => (
            <div className='comment' key={index}>
                <p className='comment-header'>{v.author} 说:</p>
                <p className='comment-content'>{v.content}</p>
                <div className='comment-footer'>
                    <div className='comment-appraise'
                        id = {v.id} 
                        onClick={this.doAppraise(this, true)}>
                        <span className='icon-like'></span>
                        {v.favour}
                    </div>
                    <div className='comment-appraise'
                        id = {v.id} 
                        onClick={this.doAppraise(this, false)}>
                        <span className='icon-dislike'></span>
                        {v.dislike}
                    </div>
                    <p className='comment-date'>{v.date}</p>
                </div>
                
            </div>
        ));

        return(
                <div id='comments'>
                    {commentNodes}
                    <AddComment 
                        fileName={this.props.fileName}
                        afterSubmit={this.getAndSetComments}
                    ></AddComment>
                </div>
        );
    }
}