import React from "react";
import { Comment } from "./Comment";

export class DefaultMain extends React.Component {
    render() {
        return (
            <div id="main" className="right">
                <h1>关于这个网站</h1>
                <p className='introduce'>这是 LLFish 用于展示笔记的网址。目前的笔记使用 mardkown 格式编辑和展示，并保存于本地文件（非数据库）。</p>
                <p className='introduce'>你可以使用微信支持，谢谢!</p>
                <div>
                    <img id='qr-code' src="/assets/QRcode.png" alt="微信二维码"></img>
                </div>
                <Comment fileName='index' />
            </div>
        );
    }
}