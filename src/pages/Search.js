import axios from "axios";
import React from "react";
import { Link } from "react-router-dom";

function SearchInfo(props) {
    return (
        <div className='searchInfo'>
            <h1>{props.fileName}</h1>
            <p>{props.content}</p>
        </div>
    );
}

export class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {success:false, errContent:'没有相关内容: '};
    }


    componentDidMount() {
        this.getSearchOverview();
    }

    componentDidUpdate(preProps) {
        const params = this.props.match.params.content;
        const preParams = preProps.match.params.content;
        if(params === preParams) {
            return;
        }
        this.getSearchOverview(); 
    }


    getSearchOverview() {
        const params = this.props.match.params.content;

        axios.get(`/search/overview?keyword=${params}`)
            .then( data => {
                let overview = data.data;
                let success = false;
                if(overview.length) {
                    success = true;
                }

                this.setState({overview, success});
            })
    }


    render() {
        const params = this.props.match.params.content;

        // debugger

        if(!this.state.success) {
            return (
                <div id="main" className="right">
                    <h1>{this.state.errContent}{params}</h1>
                </div>
            );
        }

        
        let infoNodes = [];   

        this.state.overview.forEach(element => {
            let infoNode = (
                <Link key={element.fileName} to={`/file/${element.fileName}`}>
                    <SearchInfo 
                        fileName={element.fileName}
                        content={element.content}
                    ></SearchInfo>
                </Link>
            )
            infoNodes.push(infoNode);
        }); 

        return (
            <div id="main" className="right">
                <h1>关键字：{params}</h1>
                <div id="searchInfos">
                    {infoNodes}
                </div>
            </div>
        );
    }
}

