# ShowNotes FrontEnd React

这是 show-notes 项目中 前端 react 版本的实现



# 需求

- [ ] 评论点赞与回复，参考antd
- [ ] 回到顶部
- [ ] 头像功能，暂时放文字
- [ ] 不同账号权限



# 发展目标

  - 状态管理
    - [ ] redux 
  - 使用 UI 组件
    - [ ] ant-design 
  - 工程化
    - [ ] webpack
    - [ ] babel

